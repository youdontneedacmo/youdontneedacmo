Most marketing firms have either a highly specialized, niche focus (say dentist marketing) or a McDonalds outsourcing model. We take the middle road by combining a team of ex-startup CMOs who manage teams of dozens more.

Yes, there is value to having an internal marketing teambut if you havent had much success in the past, there is little reason to take the risk on adding new members to the team, especially if you are trying to retain equity in your next fundraising round.

Website : http://youdontneedacmo.com